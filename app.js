
// Import all external modules
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
var Sequelize = require('sequelize');

// Import internal app dependencies 
const keys = require('./config/keys');


// Initialize express middleware 
const app = express();
var cors = require('cors')

// MySql Connection
const sequelize = new Sequelize(keys.MySQL.path, { operatorsAliases: 0, pool: { "acquire": 120000 } });


// if force set to true drops all data from all the tables
// sequelize.sync({
//   force: true
// }).catch('CAUGHT')


sequelize.authenticate().then(() => {
  console.log('Connection established successfully on');
}).catch(err => {
  console.error('Unable to connect to the database:', err);
}).finally(() => {
  sequelize.close();
});

app.use(cors())

// Enable CORS
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, enctype");
  next();
});

// Setup static public directory
app.use(express.static(__dirname + '/public'));

// Add body-parser 
app.use(bodyParser.json({ limit: '10mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '10mb', extended: true }))


// Add all roues here
// Added below test route
app.get('/', function (req, res) {
  res.send("App Working.....");
});

app.use('/slots', require('./routes/slots'));
app.use('/levels', require('./routes/levels'));
app.use('/rows', require('./routes/rows'));
app.use('/parkedVehicles', require('./routes/parkedVehicles'));





//Start server
const PORT = process.env.PORT || 8080;
app.listen(PORT);

console.log('server is running at port :' + PORT)
