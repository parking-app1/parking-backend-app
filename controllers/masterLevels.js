const db = require('../models/index');
const GLOBALS = require('../globals/constants');
module.exports = {
    
    initializeLevels: async (req, res, next) => {
        db.masterLevels.bulkCreate(GLOBALS.LEVELS)
            .then(result => {
                res.status(200).json({
                    status: true,
                    message: "Master levels initialized",
                    data: {}
                })
            }).catch(error => {
                return res.status(500).json({
                    status: false,
                    message: error.message,
                    data: {}
                })
            });
    }
}