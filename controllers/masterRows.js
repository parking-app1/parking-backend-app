const GLOBALS = require('../globals/constants');
const db = require('../models/index');

module.exports = {
    initializeRows: async (req, res, next) => {
        db.masterRows.bulkCreate(GLOBALS.ROWS)
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: 'Master rows initialized successfully',
                    data: {}
                });
            })
            .catch((err) => {
                res.status(500).json({
                    status: false,
                    message: err.message,
                    data: {}
                });
            });

    }
}