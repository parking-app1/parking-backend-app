const db = require('../models/index');

module.exports = {


    search: async (req, res, next) => {
        // let { vehicleType, slotId, noOfSlots } = req.body;
        let { registrationNo } = req.body;


        db.parkedVehicles.findAll(
            {
                where: { registrationNo: registrationNo }
            })
            .then((vehicle) => {
                db.slots.findAll(
                    {
                        where: { slotId: vehicle[0].slotId }
                    }
                ).then((result) => {

                    res.status(200).json({
                        status: true,
                        message: 'success',
                        data: { vehicle, result }
                    });
                }).catch((error) => {
                    res.status(500).json({
                        status: false,
                        message: error.message,
                        data: {}
                    });
                });

            })
            .catch((error) => {
                res.status(500).json({
                    status: false,
                    message: error.message,
                    data: {}
                });
            });
    },

    getAllCount: async (req, res, next) => {
        db.parkedVehicles.findAll({
            include: [
                { model: db.slots }
            ],
        })
            .then((result) => {
                res.status(200).json({
                    status: true,
                    message: 'success',
                    data: { result }
                });
            })
            .catch((error) => {
                res.status(500).json({
                    status: false,
                    message: error.message,
                    data: {}
                });
            });
    },

    unParkVehicle: async (req, res, next) => {
        let { vehicleType, noOfSlots, slotId } = req.body;
    },



    getParkingSlot: async (req, res, next) => {
        let { vehicleType, registrationNo, noOfSlots } = req.body;
        let query = `
        ;WITH  partitioned AS(
        SELECT
            *,
            slotNumber - ROW_NUMBER() OVER (PARTITION BY  levelId,rowId ORDER BY slotNumber) 
        AS grp
        FROM slots
        WHERE status = 'FREE'
            AND (
                ('${vehicleType}' = 'M' AND (slotType = 'M' OR slotType = 'C' OR slotType = 'L'))
                OR ('${vehicleType}' = 'C' AND (slotType = 'C' OR slotType = 'L'))
                OR (slotType = 'L'))
            ),
        counted AS
        (
            SELECT
             *,
             COUNT(*) OVER (PARTITION BY levelId,rowId,grp) AS cnt
            FROM partitioned
        )
        UPDATE S
        SET S.status = 'Assigned'
        OUTPUT  INSERTED.slotId, INSERTED.levelId, INSERTED.rowId, INSERTED.slotNumber
        FROM slots S
        JOIN (
            SELECT TOP ${noOfSlots}
            *
        FROM counted
        WHERE cnt >= ${noOfSlots}) toBeAssigned
        ON toBeAssigned.slotId = S.slotId;`

        db.sequelize.query(query, {
            type: db.sequelize.QueryTypes.SELECT
        }).then((freeSlot) => {

            if (freeSlot && freeSlot.length > 0) {

                let slot = `L:${freeSlot[0]['levelId']}/R:${freeSlot[0]['rowId']}/SlotNo:${freeSlot[0]['slotNumber']}`
                let slotMessage = `Slot assigned: Level - ${freeSlot[0]['levelId']}, Row - ${freeSlot[0]['rowId']}, SlotNo - ${freeSlot[0]['slotNumber']}`

                db.parkedVehicles.create(
                    {
                        slotId: freeSlot[0]['slotId'],
                        slotNumber: freeSlot[0]['slotNumber'],
                        registrationNo: registrationNo,
                        vehicleType: vehicleType
                    },
                ).then((result) => {

                    res.status(200).json({
                        status: true,
                        message: slotMessage,
                        data: { freeSlot: slot }
                    });
                }).catch((error) => {
                    res.status(500).json({
                        status: false,
                        message: error.message,
                        data: {}
                    });
                });


                res.status(200).json({
                    status: true,
                    message: 'success',
                    data: { freeSlot: slot }
                });
            } else {
                res.status(500).json({
                    status: false,
                    message: `Parking for type ${vehicleType} is full`,
                    data: {}
                });
            }
        }).catch((error) => {
            res.status(500).json({
                status: false,
                message: error.message,
                data: {}
            });
        })
    },
}
