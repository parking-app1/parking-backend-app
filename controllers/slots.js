const db = require('../models/index');
const GLOBALS = require('../globals/constants');
module.exports = {

    initializeSlots: async (req, res, next) => {

        db.slots.bulkCreate(GLOBALS.SLOTS)
            .then(result => {
                res.status(200).json({
                    status: true,
                    message: "Master slots initialized",
                    data: {}
                })
            }).catch(error => {
                return res.status(500).json({
                    status: false,
                    message: error.message,
                    data: {}
                })
            });
    },

    getFreeSlot: async (req, res, next) => {


    }
}