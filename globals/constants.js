module.exports = {
    LEVELS: [
        { level: 1 },
        { level: 2 },
        { level: 3 }
    ],
    ROWS: [
        { row: 1 },
        { row: 2 }
    ],
    SLOTS: [
        // level 1 row 1
        { levelId: 1, rowId: 1, slotNumber: 1, slotType: 'L' },
        { levelId: 1, rowId: 1, slotNumber: 2, slotType: 'L' },
        { levelId: 1, rowId: 1, slotNumber: 3, slotType: 'L' },
        { levelId: 1, rowId: 1, slotNumber: 4, slotType: 'L' },
        { levelId: 1, rowId: 1, slotNumber: 5, slotType: 'L' },
        { levelId: 1, rowId: 1, slotNumber: 6, slotType: 'L' },
        { levelId: 1, rowId: 1, slotNumber: 7, slotType: 'L' },
        { levelId: 1, rowId: 1, slotNumber: 8, slotType: 'L' },
        { levelId: 1, rowId: 1, slotNumber: 9, slotType: 'L' },
        { levelId: 1, rowId: 1, slotNumber: 10, slotType: 'L' },
        // level 1 row 2
        { levelId: 1, rowId: 2, slotNumber: 1, slotType: 'M' },
        { levelId: 1, rowId: 2, slotNumber: 2, slotType: 'M' },
        { levelId: 1, rowId: 2, slotNumber: 3, slotType: 'M' },
        { levelId: 1, rowId: 2, slotNumber: 4, slotType: 'M' },
        { levelId: 1, rowId: 2, slotNumber: 5, slotType: 'M' },
        { levelId: 1, rowId: 2, slotNumber: 6, slotType: 'C' },
        { levelId: 1, rowId: 2, slotNumber: 7, slotType: 'C' },
        { levelId: 1, rowId: 2, slotNumber: 8, slotType: 'C' },
        { levelId: 1, rowId: 2, slotNumber: 9, slotType: 'C' },
        { levelId: 1, rowId: 2, slotNumber: 10, slotType: 'C' },
        // level 2 row 1
        { levelId: 2, rowId: 1, slotNumber: 1, slotType: 'L' },
        { levelId: 2, rowId: 1, slotNumber: 2, slotType: 'L' },
        { levelId: 2, rowId: 1, slotNumber: 3, slotType: 'L' },
        { levelId: 2, rowId: 1, slotNumber: 4, slotType: 'L' },
        { levelId: 2, rowId: 1, slotNumber: 5, slotType: 'L' },
        { levelId: 2, rowId: 1, slotNumber: 6, slotType: 'L' },
        { levelId: 2, rowId: 1, slotNumber: 7, slotType: 'L' },
        { levelId: 2, rowId: 1, slotNumber: 8, slotType: 'L' },
        { levelId: 2, rowId: 1, slotNumber: 9, slotType: 'L' },
        { levelId: 2, rowId: 1, slotNumber: 10, slotType: 'L' },
        // level 3 row 2
        { levelId: 2, rowId: 2, slotNumber: 1, slotType: 'M' },
        { levelId: 2, rowId: 2, slotNumber: 2, slotType: 'M' },
        { levelId: 2, rowId: 2, slotNumber: 3, slotType: 'M' },
        { levelId: 2, rowId: 2, slotNumber: 4, slotType: 'M' },
        { levelId: 2, rowId: 2, slotNumber: 5, slotType: 'M' },
        { levelId: 2, rowId: 2, slotNumber: 6, slotType: 'C' },
        { levelId: 2, rowId: 2, slotNumber: 7, slotType: 'C' },
        { levelId: 2, rowId: 2, slotNumber: 8, slotType: 'C' },
        { levelId: 2, rowId: 2, slotNumber: 9, slotType: 'C' },
        { levelId: 2, rowId: 2, slotNumber: 10, slotType: 'C' },
        // level 3 row 1
        { levelId: 3, rowId: 1, slotNumber: 1, slotType: 'L' },
        { levelId: 3, rowId: 1, slotNumber: 2, slotType: 'L' },
        { levelId: 3, rowId: 1, slotNumber: 3, slotType: 'L' },
        { levelId: 3, rowId: 1, slotNumber: 4, slotType: 'L' },
        { levelId: 3, rowId: 1, slotNumber: 5, slotType: 'L' },
        { levelId: 3, rowId: 1, slotNumber: 6, slotType: 'L' },
        { levelId: 3, rowId: 1, slotNumber: 7, slotType: 'L' },
        { levelId: 3, rowId: 1, slotNumber: 8, slotType: 'L' },
        { levelId: 3, rowId: 1, slotNumber: 9, slotType: 'L' },
        { levelId: 3, rowId: 1, slotNumber: 10, slotType: 'L' },
        // level 3 row 2
        { levelId: 3, rowId: 2, slotNumber: 1, slotType: 'M' },
        { levelId: 3, rowId: 2, slotNumber: 2, slotType: 'M' },
        { levelId: 3, rowId: 2, slotNumber: 3, slotType: 'M' },
        { levelId: 3, rowId: 2, slotNumber: 4, slotType: 'M' },
        { levelId: 3, rowId: 2, slotNumber: 5, slotType: 'M' },
        { levelId: 3, rowId: 2, slotNumber: 6, slotType: 'C' },
        { levelId: 3, rowId: 2, slotNumber: 7, slotType: 'C' },
        { levelId: 3, rowId: 2, slotNumber: 8, slotType: 'C' },
        { levelId: 3, rowId: 2, slotNumber: 9, slotType: 'C' },
        { levelId: 3, rowId: 2, slotNumber: 10, slotType: 'C' }
    ]
}