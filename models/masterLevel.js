module.exports = (sequelize, DataTypes) => {
    var MasterLevels = sequelize.define('masterLevels', {
        levelId: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        level: {
            type: DataTypes.STRING,
            required: true
        }
    }, {
        timeStamps: true
    });


    MasterLevels.sync({
        alter: true
    }).then(() => {
    }).catch((err) => {
    });

    return MasterLevels;
}