module.exports = (sequelize, DataTypes) => {
    var MasterRows = sequelize.define('masterRows', {
        rowId: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        row: {
            type: DataTypes.STRING,
            required: true
        }
    }, {
        timeStamps: true
    });

    MasterRows.sync({
        alter: true
    }).then(() => {
    }).catch((err) => {
    });

    return MasterRows;


}