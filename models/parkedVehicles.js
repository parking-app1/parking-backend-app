module.exports = (sequelize, DataTypes) => {
    var ParkedVehicles = sequelize.define('parkedVehicles', {
        parkedVehicleId: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        slotId: {
            type: DataTypes.INTEGER
        },
        registrationNo: {
            type: DataTypes.STRING
        },
        vehicleType: {
            type: DataTypes.STRING
        },
    }, {
        timeStamps: true
    });


    ParkedVehicles.sync({
        alter: true
    }).then(() => {
    }).catch((err) => {
    });

    return ParkedVehicles;

}