module.exports = (sequelize, DataTypes) => {
    var Slots = sequelize.define('slots', {
        slotId: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        levelId: {
            type: DataTypes.STRING
        },
        rowId: {
            type: DataTypes.STRING
        },
        slotNumber: {
            type: DataTypes.INTEGER
        },
        slotType: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.ENUM,
            values: ['Free', 'Assigned'],
            defaultValue: 'Free'
        }
    }, {
        timestamps: true
    });

    Slots.sync({
        alter: true
    }).then(() => {
        // console.log('New  table created')
    }).catch((err) => {
        console.log('error while generating slots table');
    });

    return Slots;
}