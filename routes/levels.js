const express = require('express');
const router = express.Router();
const levelsController = require('../controllers/masterLevels');

router.route('/initializeLevels')
    .get(levelsController.initializeLevels);

module.exports = router;
