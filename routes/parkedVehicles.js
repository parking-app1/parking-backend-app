const express = require('express');
const router = express.Router();
const parkedVehicleController = require('../controllers/parkedVehicles');

router.route('/search')
    .post(parkedVehicleController.search);

router.route('/getParkingSlot')
    .post(parkedVehicleController.getParkingSlot);

router.route('/unParkVehicle')
    .post(parkedVehicleController.unParkVehicle);

router.route('/getAllCount')
    .get(parkedVehicleController.getAllCount);

module.exports = router;