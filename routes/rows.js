const express = require('express');
const router = express.Router();
const masterRowsController = require('../controllers/masterRows');

router.route('/initializeRows')
    .get(masterRowsController.initializeRows);

module.exports = router;