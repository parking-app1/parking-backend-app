const express = require('express');
const router = express.Router();
const slotsController = require('../controllers/slots');

router.route('/getFreeSlot')
    .post(slotsController.getFreeSlot)

router.route('/initializeSlots')
    .get(slotsController.initializeSlots);

module.exports = router;
